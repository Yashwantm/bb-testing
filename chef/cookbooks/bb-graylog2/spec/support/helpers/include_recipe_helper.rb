# Updated for rspec 3.0:
module IncludeRecipeHelper
  def enable_stubbed_include_recipe # rubocop:disable Metrics/AbcSize
    # Don't worry about external cookbook dependencies
    allow_any_instance_of(Chef::Cookbook::Metadata).to receive(:depends)

    # Test each recipe in isolation, regardless of includes
    @included_recipes = []
    allow_any_instance_of(Chef::RunContext).to receive(:loaded_recipe?).and_return(false)
    allow_any_instance_of(Chef::Recipe).to receive(:include_recipe) do |_recipe, included_recipe|
      allow_any_instance_of(Chef::RunContext).to receive(:loaded_recipe?).with(included_recipe).and_return(true)
      @included_recipes << included_recipe
    end
    allow_any_instance_of(Chef::RunContext).to receive(:loaded_recipes).and_return(@included_recipes)
  end
end

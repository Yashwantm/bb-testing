#
# Cookbook Name:: bb-graylog2
# Spec:: default
#
# Copyright (c) 2016 Tacit Knowledge, All Rights Reserved.

require 'spec_helper'

describe 'bb-graylog2::default' do
  context 'When all attributes are default, on ubuntu 14.04' do
    before do
      enable_stubbed_include_recipe
    end

    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '14.04')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end

require 'chefspec'
require 'chefspec/berkshelf'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir['./spec/support/**/*.rb'].each { |f| require f }

RSpec.configure do |config|
  config.log_level = :error
  config.include IncludeRecipeHelper
end

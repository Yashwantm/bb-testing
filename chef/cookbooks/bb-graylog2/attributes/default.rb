default['java']['jdk_version'] = '8'
default['java']['install_flavor'] = 'oracle'
default['java']['oracle']['accept_oracle_download_terms'] = true

default['elasticsearch']['version'] = '5.0.1'
default['elasticsearch']['cluster']['name'] = 'graylog2'

default['graylog2']['server']['version'] = '2.1.2-1'
default['graylog2']['root_timezone'] = 'UTC'

default['graylog2']['password_secret'] = '$6$dqoy74p6$fWpmgASqkJk5sx8UvNi0knVGsG5ryjgLiYy4t2ye5IC1lJmFSR7gAflHs8ae1/ZzaeqcgYl.m0tMZh2QnFldf0'
default['graylog2']['root_password_sha2'] = '0e2d2cead140da1cc4a2fb47494a4080ff488c233215296b1ca168f1f5b54769'
default['graylog2']['web']['secret'] = '$6$dqoy74p6$fWpmgASqkJk5sx8UvNi0knVGsG5ryjgLiYy4t2ye5IC1lJmFSR7gAflHs8ae1/ZzaeqcgYl.m0tMZh2QnFldf0'

default['graylog2']['web']['endpoint_uri'] = "http://#{node.chef_environment}-graylog2.y.balsamhill.com:9000/api"

default['graylog2']['elasticsearch']['cluster_name'] = 'graylog2'

default['graylog2']['server']['java_opts'] = '-Djava.net.preferIPv4Stack=true'
default['elasticsearch']['allocated_memory'] = '1024'
default['elasticsearch']['instance_name'] = 'elasticsearch'
default['elasticsearch']['user'] = 'elasticsearch'


Cookbook Name:: bb-graylog2
Recipe:: default

Copyright (c) 2016 Tacit Knowledge, All Rights Reserved.
include_recipe 'apt'
include_recipe 'bb-java::java'
include_recipe 'bb-graylog2::elasticsearch'
include_recipe 'mongodb'
include_recipe 'graylog2'
include_recipe 'graylog2::server'

#TODO: automatize the creation of 2 inputs on graylog2, 1 gelf udp port 12201
#another tcp syslog on port 5959

elasticsearch_user node['elasticsearch']['user']

elasticsearch_install node['elasticsearch']['instance_name'] do
  type 'tarball' # type of install
  version node['elasticsearch']['version']
  download_url 'https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.4/elasticsearch-2.4.4.tar.gz'
  action :remove # could be :remove as well
end

elasticsearch_configure node['elasticsearch']['instance_name'] do
  allocated_memory node['elasticsearch']['allocated_memory']
  configuration('cluster.name' => node['elasticsearch']['cluster']['name'])
end

elasticsearch_service node['elasticsearch']['instance_name'] do
  init_source 'elasticsearch.erb'
  init_cookbook 'bb-graylog2'
end

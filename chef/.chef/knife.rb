require 'pathname'
current_dir = Pathname.new(__FILE__).realpath.dirname.to_s
log_level                 :info
log_location              STDOUT
chef_server_url           'https://52.53.188.102/organizations/balsambrands'
cache_type                'BasicFile'
cache_options(path: File.expand_path(File.join(current_dir, '/../checksums'), __FILE__))
cookbook_path             [ 'cookbooks', 'site-cookbooks', File.join(current_dir, '../.berkshelf/cookbooks') ]
ssl_verify_mode           :verify_none
node_name                 'jenkins'
client_key                File.join(current_dir, 'jenkins.pem') # place it manually

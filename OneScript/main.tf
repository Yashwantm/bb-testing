provider "aws" {
  region = "us-west-1"
  shared_credentials_file = "c:\\users\\yashwant.mahawar\\.aws\\credentials"
  profile = "balsam"
}

resource "aws_instance" "ec2" {
  ami = "ami-069339bea0125f50d" #ubuntu 16.04
  associate_public_ip_address = true
  instance_type = "t2.large"
  key_name = "BBDevOps"
  security_groups = ["sg-0e1a5848ea8edae66"]
  subnet_id = "subnet-3cf16c59"

tags {
  Name = "Yashwant_16_04_testing"
}
  provisioner "chef" {
    environment     = "infra_test"
    run_list        = ["role[]"]
    node_name       = "Yashwant_ES_test"
    server_url      = "https://52.53.188.102/organizations/balsambrands"
    secret_key      = "${file("~/.chef/encrypted_data_bag_secret_avalara-test")}"
    user_key        = "${file("~/.chef/jenkins.pem")}"
    user_name       = "jenkins"
    recreate_client = true
    ssl_verify_mode = ":verify_none"
    version         = "12.19.36"

    connection {
      user        = "ubuntu"
      host        = "${self.public_ip}"
      private_key = "${file("~/.chef/BBDevOps.pem")}"
    }
}
}
